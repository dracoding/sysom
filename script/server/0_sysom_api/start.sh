#!/bin/bash
SERVICE_NAME=sysom-api
start_app() {
    for service in `supervisorctl status | grep ${SERVICE_NAME} | awk '{print $1}'`
    do
        supervisorctl start $service
    done
}

start_app
